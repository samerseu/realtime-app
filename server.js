//============== fresh code (Khokon - 12-12-2021)
const app = require('express')();
const http = require('http').Server(app);
const cors = require('cors');
//const io = require('socket.io')(http);
const io = require("socket.io")(http, 
                 {  cors: {    origin: "*",    methods: ["GET", "POST"]  }
            });
const axios = require('axios');

app.use(cors());
app.options('*', cors());


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

 // Add this
 if (req.method === 'OPTIONS') {
      res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, OPTIONS');
      res.header('Access-Control-Max-Age', 120);
      return res.status(200).json({});
  }

  next();

});


app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

//Whenever someone connects this gets executed
io.on('connection', function(socket){
   console.log('A user connected');
   setTimeout(() => { getTrakers() }, 1000)
   //Whenever someone disconnects this piece of code executed
   socket.on('disconnect', function () {
      console.log('A user disconnected');
   });
});

let getTrakers = () => {
    let url = 'https://freecurrencyapi.net/api/v2/latest?apikey=4d2b1a20-68c3-11ec-8744-472ec523a01f';
    axios.get(url)
        .then(function (trackers) {
            // handle success
             console.log('got data: ' + trackers.data.data);
            io.emit('trackers', trackers.data.data);
        }).catch( (err) => {
            console.log(`error: ${err}`)
        }).then(() => {
            console.log('finally')
        });
}

 

setTimeout(getTrakers, 1000);

//httpServer.listen(3000);

http.listen(3000, function(){ console.log('listening on *:3000');  });




